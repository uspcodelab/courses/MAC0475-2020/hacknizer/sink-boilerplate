import * as natsStreaming from 'node-nats-streaming';

import { topicCallback } from './topicCallback';

import { setupDB } from './db';

const { BROKER_URL, BROKER_CLUSTER_ID } = process.env;

const stan = natsStreaming.connect(BROKER_CLUSTER_ID, 'sink-boilerplate', {
  url: BROKER_URL,
});

stan.on('connect', async () => {
  const db = await setupDB();

  const replayAllOpt = stan.subscriptionOptions().setDeliverAllAvailable();
  const demoSubs = stan.subscribe('topic', replayAllOpt);

  setTimeout(() => demoSubs.unsubscribe(), 1500);

  demoSubs.on('unsubscribed', () => stan.close());

  demoSubs.on('message', topicCallback);

  stan.publish('topic', 'hello, world!');
});

stan.on('close', () => process.exit());
